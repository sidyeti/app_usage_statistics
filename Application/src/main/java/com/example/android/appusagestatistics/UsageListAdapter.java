/*
* Copyright (C) 2014 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.example.android.appusagestatistics;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Provide views to RecyclerView with the directory entries.
 */
public class UsageListAdapter extends RecyclerView.Adapter<UsageListAdapter.ViewHolder> {

    private List<CustomUsageStats> mCustomUsageStatsList = new ArrayList<>();
    private DateFormat mDateFormat = new SimpleDateFormat();
    private Context context;

    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final TextView mPackageName;
        private final TextView mLastTimeUsed;
        private final ImageView mAppIcon;


        ViewHolder(View v) {
            super(v);
            mPackageName = v.findViewById(R.id.textview_package_name);
            mLastTimeUsed = v.findViewById(R.id.textview_last_time_used);
            mAppIcon = v.findViewById(R.id.app_icon);
            mAppIcon.setOnClickListener(this);
        }

        TextView getLastTimeUsed() {
            return mLastTimeUsed;
        }

        TextView getPackageName() {
            return mPackageName;
        }

        ImageView getAppIcon() {
            return mAppIcon;
        }

        @Override
        public void onClick(View v) {
            Log.d("CLICKED", String.valueOf(mCustomUsageStatsList.get(getAdapterPosition()).usageStats.getTotalTimeInForeground()));
            long lastTimeUsed = mCustomUsageStatsList.get(getAdapterPosition()).usageStats.getLastTimeUsed();
           // String lastTime=mDateFormat.format(new Date(lastTimeUsed));
            long totalTimeUsed=mCustomUsageStatsList.get(getAdapterPosition()).usageStats.getTotalTimeInForeground();
           // String totalTime=String.format("%02d min, %02d sec", TimeUnit.MILLISECONDS.toMinutes(totalTimeUsed),
           //         TimeUnit.MILLISECONDS.toSeconds(totalTimeUsed) -
           //                 TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(totalTimeUsed)));
            String appName=mCustomUsageStatsList.get(getAdapterPosition()).usageStats.getPackageName();
            Drawable icon=mCustomUsageStatsList.get(getAdapterPosition()).appIcon;
//            Bitmap iconBitmap=((BitmapDrawable) icon).getBitmap();
            Toast.makeText(v.getContext(), mCustomUsageStatsList.get(getAdapterPosition()).interval, Toast.LENGTH_SHORT).show();

            Intent intent =new Intent(context,AppDetailsActivity.class);
            intent.putExtra("lastTime",lastTimeUsed);
            intent.putExtra("totalTime",totalTimeUsed);
            intent.putExtra("appName",appName);
            intent.putExtra("interval",mCustomUsageStatsList.get(getAdapterPosition()).interval);
            Bundle bundle=new Bundle();
//            bundle.putParcelable("icon",iconBitmap);
            intent.putExtra("icon_bundle",bundle);
            context.startActivity(intent);


        }
    }

    public UsageListAdapter(Context context) {
        this.context=context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.usage_row, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        viewHolder.getPackageName().setText(
                mCustomUsageStatsList.get(position).usageStats.getPackageName());
        long lastTimeUsed = mCustomUsageStatsList.get(position).usageStats.getLastTimeUsed();
        viewHolder.getLastTimeUsed().setText(mDateFormat.format(new Date(lastTimeUsed)));
        viewHolder.getAppIcon().setImageDrawable(mCustomUsageStatsList.get(position).appIcon);
    }

    @Override
    public int getItemCount() {
        return mCustomUsageStatsList.size();
    }

    public void setCustomUsageStatsList(List<CustomUsageStats> customUsageStats) {
        mCustomUsageStatsList = customUsageStats;
    }
}