package com.example.android.appusagestatistics;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class AppDetailsActivity extends AppCompatActivity {

    String interval;
    long totalTimeUsed;
    long lastTime;
    String appNameText;
    private DateFormat mDateFormat = new SimpleDateFormat();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_details);
        TextView lastUsageView= (TextView) findViewById(R.id.last_usage_value);
        TextView totalForeground= (TextView) findViewById(R.id.total_time_foreground);
        TextView appName= (TextView) findViewById(R.id.app_name_text_view);
        ImageView appIcon= (ImageView) findViewById(R.id.app_icon_image_view);
        appNameText=getIntent().getStringExtra("appName");
        appName.setText(getIntent().getStringExtra("appName"));

        String lastTime=mDateFormat.format(new Date(getIntent().getLongExtra("lastTime",0)));
        lastUsageView.setText(lastTime);
        totalTimeUsed=getIntent().getLongExtra("totalTime",0);
        String totalTime=String.format("%02d min, %02d sec", TimeUnit.MILLISECONDS.toMinutes(totalTimeUsed),
                        TimeUnit.MILLISECONDS.toSeconds(totalTimeUsed) -
                                 TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(totalTimeUsed)));
        totalForeground.setText(totalTime);
        interval=getIntent().getStringExtra("interval");
        Button button = (Button) findViewById(R.id.details_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(AppDetailsActivity.this,ChartActivity.class);
                intent.putExtra("interval",interval);
                Log.d("TOT_TIME_INTENT_DATA", String.valueOf(totalTimeUsed));
                intent.putExtra("timeused",totalTimeUsed);
                intent.putExtra("appName",appNameText);
                Toast.makeText(AppDetailsActivity.this, "Clicked", Toast.LENGTH_SHORT).show();
                startActivity(intent);
            }
        });

        try {
            Drawable appIconDrawable = getPackageManager()
                    .getApplicationIcon(getIntent().getStringExtra("appName"));
            appIcon.setImageDrawable(appIconDrawable);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }
}
