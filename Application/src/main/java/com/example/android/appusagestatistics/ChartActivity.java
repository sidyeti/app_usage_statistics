package com.example.android.appusagestatistics;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ChartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart);
        PieChart pieChart= (PieChart) findViewById(R.id.pie_chart);
        List<PieEntry> entries = new ArrayList<>();
        Float data1= (float) getIntent().getLongExtra("timeused", 0);
        long days=TimeUnit.MILLISECONDS.toHours(getIntent().getLongExtra("timeused", 0));
        TextView tv= (TextView) findViewById(R.id.app_name_in_chart);
        Log.d("DAYS", String.valueOf(days));
        tv.setText(getIntent().getStringExtra("appName"));
        Float data2=0f;
        String interval=getIntent().getStringExtra("interval");
        Log.d("INTERVAL",interval);
        switch (interval){
            case "Daily":
                days=TimeUnit.MILLISECONDS.toMinutes(getIntent().getLongExtra("timeused",0));
                data2= (float) TimeUnit.DAYS.toMinutes(1); break;
            case "Weekly":
                data2= (float) TimeUnit.DAYS.toHours(7); break;
            case "Monthly":
                data2= (float) TimeUnit.DAYS.toHours(30); break;
            case "Yearly":
                data2= (float) TimeUnit.DAYS.toHours(365); break;

        }
        entries.add(new PieEntry(days, "App used"));
        entries.add(new PieEntry(data2, "total time"));

        PieDataSet set = new PieDataSet(entries, "App usage information");

        set.setColors(Color.parseColor("#ef5350"),Color.parseColor("#039BE5"),Color.parseColor("#00E676"),Color.parseColor("#FBC02D"));
        PieData data = new PieData(set);
        pieChart.setData(data);
        pieChart.setCenterText(interval);
        pieChart.invalidate();

    }
}
